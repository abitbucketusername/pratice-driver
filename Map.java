package pack.main;

import java.awt.Color;
import java.awt.Graphics;

public class Map extends Drawer
{
	int x, y;
	
	public Map()
	{
		x=20;
		y=30;
	}

	@Override
	public void drawing(Graphics g)
	{
		g.setColor(Color.BLUE);
		g.fillRect(x,y,50,300);
	}

}