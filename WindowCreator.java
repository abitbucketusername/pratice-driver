package pack.main;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public class WindowCreator
{
	public WindowCreator(){}
	
	public void windowSetUp(JFrame frame, Canvas canvas, int width, int height)
	{
		Dimension dimensionOfWindow = new Dimension(width, height);
		frame.setMinimumSize(dimensionOfWindow);
		frame.setMaximumSize(dimensionOfWindow);
		frame.setPreferredSize(dimensionOfWindow);
		frame.setTitle(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setLayout(new BorderLayout());
		
		frame.add(canvas, BorderLayout.CENTER);
		
		frame.pack();
		
		frame.setVisible(true);
	}

	public void windowSetUp(JFrame frame, Canvas canvas, int width, int height, int scale)
	{
		Dimension dimensionOfWindow = new Dimension(width*scale, height*scale);
		frame.setMinimumSize(dimensionOfWindow);
		frame.setMaximumSize(dimensionOfWindow);
		frame.setPreferredSize(dimensionOfWindow);
		frame.setTitle(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setLayout(new BorderLayout());
		
		frame.add(canvas, BorderLayout.CENTER);
		
		frame.pack();
		
		frame.setVisible(true);
	}

	public void windowSetUp(JFrame frame, Canvas canvas, String title, int width, int height)
	{
		Dimension dimensionOfWindow = new Dimension(width, height);
		frame.setMinimumSize(dimensionOfWindow);
		frame.setMaximumSize(dimensionOfWindow);
		frame.setPreferredSize(dimensionOfWindow);
		frame.setTitle(title);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setLayout(new BorderLayout());
		
		frame.add(canvas, BorderLayout.CENTER);
		
		frame.pack();
		
		frame.setVisible(true);
	}

	public void windowSetUp(JFrame frame, Canvas canvas, String title, int width, int height, int scale)
	{
		Dimension dimensionOfWindow = new Dimension(width*scale, height*scale);
		frame.setMinimumSize(dimensionOfWindow);
		frame.setMaximumSize(dimensionOfWindow);
		frame.setPreferredSize(dimensionOfWindow);
		frame.setTitle(title);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setLayout(new BorderLayout());
		
		frame.add(canvas, BorderLayout.CENTER);
		
		frame.pack();
		
		frame.setVisible(true);
	}

	public void windowSetUp(JFrame frame, Canvas canvas, String title, int width, int height, int scale, boolean isVisible)
	{
		Dimension dimensionOfWindow = new Dimension(width*scale, height*scale);
		frame.setMinimumSize(dimensionOfWindow);
		frame.setMaximumSize(dimensionOfWindow);
		frame.setPreferredSize(dimensionOfWindow);
		frame.setTitle(title);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);

		frame.setLayout(new BorderLayout());
		
		frame.add(canvas, BorderLayout.CENTER);
		
		frame.pack();
		
		frame.setVisible(isVisible);
	}

	public void windowSetUp(JFrame frame, Canvas canvas, String title, int width, int height, int scale, boolean isVisible, boolean isResizable)
	{
		Dimension dimensionOfWindow = new Dimension(width*scale, height*scale);
		frame.setMinimumSize(dimensionOfWindow);
		frame.setMaximumSize(dimensionOfWindow);
		frame.setPreferredSize(dimensionOfWindow);
		frame.setTitle(title);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(isResizable);
		frame.setLayout(new BorderLayout());
		
		frame.add(canvas, BorderLayout.CENTER);

		frame.pack();
		
		frame.setVisible(isVisible);
	}
}
