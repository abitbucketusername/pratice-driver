package pack.main;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class DriverMain extends Canvas implements Runnable, ActionListener{

	final int WINDOW_WIDTH = 300, WINDOW_HEIGHT = WINDOW_WIDTH/16*9;
	final int WINDOW_SCALE = 3;
	final String WINDOW_TITLE = "Drive";

	private JFrame windowFrame = new JFrame();
	private WindowCreator windowCreator = new WindowCreator();
	private Canvas canvas = new Canvas();
	private Map levelOne = new Map();
	private InputHandler inputHandler = new InputHandler();
	private Timer timer = new Timer(17, this);
	
	private Graphics g = null;
	
	public boolean running = false;
	
	public DriverMain()
	{
		windowCreator.windowSetUp(windowFrame, canvas, WINDOW_TITLE, WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_SCALE, true, false);
		canvas.addKeyListener(inputHandler);
		timer.start();
	}
	
	public synchronized void start()
	{
		running = true;
		new Thread(this).start();
	}
	
	public synchronized void stop()
	{
		running = false;
	}
	
	public void update()
	{		
		if(canvas.hasFocus()){
			if(inputHandler.right.isPressed()){levelOne.x+=5;}
			if(inputHandler.left.isPressed()){levelOne.x-=5;}
			if(inputHandler.up.isPressed()){levelOne.y-=5;}
			if(inputHandler.down.isPressed()){levelOne.y+=5;}
		}
		levelOne.render(g, canvas);
		levelOne.clearWindow(canvas, windowFrame);
	}
	
	public void run()
	{
	}

	public static void main(String [] args)
	{
		new DriverMain().start();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		update();
	}
	
}
