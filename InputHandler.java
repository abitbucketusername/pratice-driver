package pack.main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class InputHandler implements KeyListener{
	
	public Key up = new Key();
	public Key down = new Key();
	public Key right = new Key();
	public Key left = new Key();
	
	public InputHandler(){}
	
	public InputHandler(DriverMain frame)
	{
		frame.addKeyListener(this);
	}
	
	public class Key
	{
		private boolean pressed = false;
		
		public boolean isPressed()
		{
			return pressed;
		}
		
		public void toggle(boolean isPressed)
		{
			pressed = isPressed;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) 
	{
		
	}

	@Override
	public void keyPressed(KeyEvent e) 
	{
		toggleKey(e.getKeyCode(), true);
	}

	@Override
	public void keyReleased(KeyEvent e) 
	{
		toggleKey(e.getKeyCode(), false);
	}
	
	public void toggleKey(int keyCode, boolean isPressed)
	{
		if(keyCode == KeyEvent.VK_W){up.toggle(isPressed);}
		if(keyCode == KeyEvent.VK_S){down.toggle(isPressed);}
		if(keyCode == KeyEvent.VK_D){right.toggle(isPressed);}
		if(keyCode == KeyEvent.VK_A){left.toggle(isPressed);}
	}
}
