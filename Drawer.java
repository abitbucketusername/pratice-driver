package pack.main;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

public class Drawer
{
	public BufferStrategy bufferStrategy;

	public int windowBuffer = 3;

	public Drawer(){}

	public void drawing(Graphics g){}
	
	public void render(Graphics g, Canvas canvas)
	{	
		bufferStrategy = canvas.getBufferStrategy();

		if(bufferStrategy == null)
		{
			canvas.createBufferStrategy(windowBuffer);
			return;
		}

		g = bufferStrategy.getDrawGraphics();

		drawing(g);
		
		g.dispose();
		bufferStrategy.show();
	}
	
	public void clearWindow(Canvas canvas, JFrame frame)
	{
		BufferStrategy bufferStrategy = canvas.getBufferStrategy();
		Graphics g;
		
		if(bufferStrategy == null)
		{
			canvas.createBufferStrategy(windowBuffer);
			return;
		}

		g = bufferStrategy.getDrawGraphics();
		
		g.clearRect(0, 0, frame.getWidth(), frame.getHeight());
		
		g.dispose();
		bufferStrategy.show();
	}
}
